/**
 * FruitResource.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.rest;

import org.jboss.logging.Logger;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.inditex.demo.domain.Fruit;
import com.inditex.demo.domain.MemoryData;
import com.inditex.demo.service.FruitService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * The Class FruitResource.
 *
 * @author capgemini
 */
@RestController
@RequestMapping(path = "/fruits")
public class FruitResource {

    private static final Logger LOGGER = Logger.getLogger("Main");

    /** The fruit service. */
    FruitService fruitService;

    public FruitResource(final FruitService fruitService) {
        this.fruitService = fruitService;
    }

    @GetMapping()
    public @ResponseBody Flux<Fruit> findFruits() {
        LOGGER.info("findFruits");
        return this.fruitService.list();
    }

    @GetMapping("/name")
    public Mono<Fruit> findByName(@PathVariable final String name) {
        LOGGER.info("findByName: " + name);
        return this.fruitService.findByName(name);
    }

    @PostMapping()
    public Mono<Fruit> create(@RequestBody final Fruit fruit) {
        LOGGER.info("create:" + fruit.getName() + " " + fruit.getDescription());
        return this.fruitService.add(fruit);
    }

    @DeleteMapping()
    public Mono<Void> delete(@PathVariable final String name) {
        LOGGER.info("delete: " + name);
        return this.fruitService.deleteByName(name);
    }

    @DeleteMapping("/all")
    public Mono<Void> deleteAll() {
        LOGGER.info("deleteAll");
        return this.fruitService.deleteAll();
    }

    /**
     * Count fruits.
     *
     * @return the long
     */
    @GetMapping("/count")
    public Mono<Long> count() {
        LOGGER.info("count");
        return this.fruitService.count();
    }

    /**
     * Memory check.
     *
     * @return the memory data
     */
    @GetMapping("/memoryCheck")
    public MemoryData memoryCheck() {
        LOGGER.info("memoryCheck");
        return this.fruitService.memoryCheck();
    }
}