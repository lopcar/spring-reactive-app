/**
 * FruitService.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.service;

import org.springframework.stereotype.Service;

import com.inditex.demo.domain.Fruit;
import com.inditex.demo.domain.MemoryData;
import com.inditex.demo.repository.FruitRepository;
import com.inditex.demo.utils.MemoryUtils;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * The Class FruitService.
 *
 * @author capgemini
 */
@Service
public class FruitService {

    /** The fruit repository. */
    FruitRepository fruitRepository;

    /**
     * Instance a new fruit service.
     *
     * @param fruitRepository
     *            fruit repository
     */
    public FruitService(final FruitRepository fruitRepository) {
        this.fruitRepository = fruitRepository;
    }

    /**
     * List.
     *
     * @return the flux
     */
    public Flux<Fruit> list() {
        return this.fruitRepository.findAll();
    }

    /**
     * Find by name.
     *
     * @param name
     *            name
     * @return the mono
     */
    public Mono<Fruit> findByName(final String name) {
        return this.fruitRepository.findByName(name);
    }

    /**
     * Adds the.
     *
     * @param fruit
     *            fruit
     * @return the mono
     */
    public Mono<Fruit> add(final Fruit fruit) {
        return this.fruitRepository.insert(fruit);
    }

    /**
     * Delete by name.
     *
     * @param name
     *            name
     * @return the mono
     */
    public Mono<Void> deleteByName(final String name) {
        return this.fruitRepository.deleteByName(name);
    }

    /**
     * Delete all.
     *
     * @return the mono
     */
    public Mono<Void> deleteAll() {
        return this.fruitRepository.deleteAll();
    }

    /**
     * Count.
     *
     * @return the mono
     */
    public Mono<Long> count() {
        return this.fruitRepository.count();
    }

    /**
     * Memory check.
     *
     * @return the memory data
     */
    public MemoryData memoryCheck() {
        return MemoryUtils.checkMemory();
    }
}
