/**
 * MemoryUtils.java 21-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.utils;

import java.text.NumberFormat;

import com.inditex.demo.domain.MemoryData;

public class MemoryUtils {

    private MemoryUtils() {}

    public static MemoryData checkMemory()
    {
        final MemoryData memoryData = new MemoryData();

        final Runtime runtime = Runtime.getRuntime();
        final NumberFormat format = NumberFormat.getInstance();

        final long maxMemory = runtime.maxMemory();
        final long allocatedMemory = runtime.totalMemory();
        final long freeMemory = runtime.freeMemory();

        final long mb = 1024L * 1024;
        final String mb_text = " MB";

        memoryData.setMaxMemory(format.format(maxMemory / mb) + mb_text);
        memoryData.setAllocatedMemory(format.format(maxMemory / mb) + mb_text);
        memoryData.setTotalFreeMemory(format.format((freeMemory + (maxMemory - allocatedMemory)) / mb) + mb_text);
        memoryData.setFreeMemory(format.format(freeMemory / mb) + mb_text);

        return memoryData;
    }

}
