/**
 * FruitRepository.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.inditex.demo.domain.Fruit;

import reactor.core.publisher.Mono;


/**
 * The Interface FruitRepository.
 *
 * @author capgemini
 */
@Repository
public interface FruitRepository extends ReactiveMongoRepository<Fruit, Long> {

    /**
     * Find by name.
     *
     * @param name
     *            name
     * @return the mono
     */
    public Mono<Fruit> findByName(String name);


    /**
     * Delete by name.
     *
     * @param name
     *            name
     * @return the mono
     */
    public Mono<Void> deleteByName(String name);
}
