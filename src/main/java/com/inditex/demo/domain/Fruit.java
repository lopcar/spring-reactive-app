/**
 * Fruit.java 08-may-2020
 *
 * Copyright 2020 INDITEX.
 * Systems Department
 */
package com.inditex.demo.domain;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Document
@Data  
@AllArgsConstructor  
@NoArgsConstructor
public class Fruit {

    public ObjectId id;

    public String name;

    public String description;

}
